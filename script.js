





let trainer = {

	name: "Ash Ketchum",

	age: 10,

	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],

	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock","Misty"]
	},
	
	talk: function(pokemonIndex){
		console.log("Pikachu! I choose you!");
	}

}

console.log("Result of dot notation");
console.log(trainer.name);

console.log("Result of bracket notation");
console.log(trainer['pokemon']);


trainer.talk();


function Pokemon(name, level) {

	this.name = name;
	this.level = level;
	this.health = 2 * level; //200
	this.attack = level; //3

	//methods/function

	this.tackle = function(target){

		console.log(this.name + " tackled " + target.name)

		target.health = target.health - this.attack;

		console.log( target.name + "'s health is now to " + target.health);


		if(target.health <= 0){
			target.faint();
		}

		console.log(target);

	}

	this.faint = function(){
			console.log(this.name + " fainted");
		}

}


let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);

mewtwo.tackle(geodude);
